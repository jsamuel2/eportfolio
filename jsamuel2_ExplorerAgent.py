from wumpus import ExplorerAgent
from sortedcollections import ValueSortedDict
from collections import deque
import random
import copy

# Represents one room of the cave
class Node():
	def __init__(self):
		self.visited = False			# True if the room has been visited before, false otherwise
		self.containsWumpus = None		# True if the room is known to house the wumpus
		self.containsPit = None			# True if the room is known to compain a pit
		self.containsBreeze = None		# True if the room has a breeze
		self.containsStench = None		# True if the room has a pit
		self.containsWall = False		# True if the room is a wall
		self.isSafe = False				# True if the room is known to not contain a pit or house the wumpus

	def checkIsSafe(self):
		if (self.visited):
			self.isSafe = True
		elif (self.containsWumpus is not None) or (self.containsPit is not None):
			self.isSafe = self.containsWumpus == False and self.containsPit == False
			if self.containsWall == True:
				self.isSafe = False

	def setVariables(self, percept):
		# If you are in the room, it has been visited and is either safe or you are dead
		self.visited = True
		self.isSafe = True
		self.containsPit = True
		self.containsWumpus = True

		# Set the other variables based on the agent's percepts
		self.containsBreeze = percept[1] == 'Breeze'
		self.containsStench = percept[0] == 'Stench'

class KB():
	def __init__(self):
		self.leaving = False				# True if the agent needs to leave the cave
		self.pickUpGold = False				# True if the agent has found the gold, but not picked it up yet
		self.agentLocation = 12				# The room currently occupied by the agent
		self.agentDirection = 'U'			# The direction the agent is currently facing
		self.nextLocation = None 			# The location of the next space the agent is moving to
		self.destination = None				# The current room that the agent is moving towards
		self.pathToDestination = []			# The path to get to destination
		self.prevLocation = None			# The room that the agent thinks it was previously in, will only be incorrect if it hits a wall

		'''
		A map showing the representing what is known about each room

		Map layout
		0  1  2  3
		4  5  6  7
		8  9  10 11
		12 13 14 15
		'''
		self.map = []

		for i in range(4):
			self.map.append( [Node(), Node(), Node(), Node()] )

		self.mapCoordinates = {
			0  : [0, 0],
			1  : [0, 1],
			2  : [0, 2],
			3  : [0, 3],
			4  : [1, 0],
			5  : [1, 1],
			6  : [1, 2],
			7  : [1, 3],
			8  : [2, 0],
			9  : [2, 1],
			10 : [2, 2],
			11 : [2, 3],
			12 : [3, 0],
			13 : [3, 1],
			14 : [3, 2],
			15 : [3, 3]
		}

		self.mapLocations = {
			"00" : 0,
			"01" : 1,
			"02" : 2,
			"03" : 3,
			"10" : 4,
			"11" : 5,
			"12" : 6,
			"13" : 7,
			"20" : 8,
			"21" : 9,
			"22" : 10,
			"23" : 11,
			"30" : 12,
			"31" : 13,
			"32" : 14,
			"33" : 15
		}

	def tell(self, percept):
		# If this is not the first move, check to make sure that the last move did not find a wall
		if percept[3] == 'Bump':
			self.map[self.mapCoordinates[self.agentLocation][0]][self.mapCoordinates[self.agentLocation][1]].containsWall = True
			self.agentLocation = self.prevLocation
			self.destination = None

		else:
		# If there was no wall, set up the prevLocation variable for the method call. It won't be needed again until then
			self.prevLocation = self.agentLocation

		# Store the information of the room currently occupied by the agent
		self.map[self.mapCoordinates[self.agentLocation][0]][self.mapCoordinates[self.agentLocation][1]].setVariables(percept)

		# If a scream is heard, mark every room as wumpus free
		if percept[4] == 'Scream':
			for i in range(4):
				for j in range(4):
					map[i][j].containsWumpus = False
					map[i][j].containsStench = False

		# If glitter is seen, the gold is found and the finishing procudure should be initiated
		if percept[2] == 'Glitter':
			self.pickUpGold = True

		# Update each room's knowledge based on the new knowledge that you have
		for i in range(4):
			for j in range(4):
				# If the room has no breeze, mark all of the adjacent squares as not containing a pit
				if self.map[i][j].containsBreeze == False:
					if i != 0:
						self.map[i - 1][j].containsPit = False
					if i != 3:
						self.map[i + 1][j].containsPit = False
					if j != 0:
						self.map[i][j - 1].containsPit = False
					if j != 3:
						self.map[i][j + 1].containsPit = False

				# If the room has no stench, mark all of the adjacent squares as not containing the wumpus
				if self.map[i][j].containsStench == False:
					if i != 0:
						self.map[i - 1][j].containsWumpus = False
					if i != 3:
						self.map[i + 1][j].containsWumpus = False
					if j != 0:
						self.map[i][j - 1].containsWumpus = False
					if j != 3:
						self.map[i][j + 1].containsWumpus = False

				# If a stench was marked and only 1 adjacent square is marked as not being wumpus-free, it's in that square
				# If the room has less than 4 adjacent squares, you can assume the nonexistent rooms contain a do not contain a wumpus
				if self.map[i][j].containsStench == True:
					if i != 0:
						roomNorth = self.map[i - 1][j].containsWumpus
					else:
						roomNorth = False
					if i != 3:
						roomSouth = self.map[i + 1][j].containsWumpus
					else:
						roomSouth = False
					if j != 0:
						roomWest = self.map[i][j - 1].containsWumpus
					else:
						roomWest = False
					if j != 3:
						roomEast = self.map[i][j + 1].containsWumpus
					else:
						roomEast = False
					# If all four are not known to be wumpus-free, check to see if you can deduce one room to house the wumpus
					if (roomNorth == False and roomSouth == False and roomEast == False and roomWest == False) == False:
						if roomNorth == False and roomSouth == False and roomEast == False:
							self.map[i][j - 1].containsWumpus = True
						elif roomNorth == False and roomSouth == False and roomWest == False:
							self.map[i][j + 1].containsWumpus = True
						elif roomNorth == False and roomEast == False and roomWest == False:
							self.map[i + 1][j].containsWumpus = True
						elif roomSouth == False and roomEast == False and roomWest == False:
							self.map[i - 1][j].containsWumpus = True

				# If a breeze was marked and only 1 adjacent square is marked as not having a pit, it must be in that square
				if self.map[i][j].containsBreeze == True:
					if i != 0:
						roomNorth = self.map[i - 1][j].containsPit
					else:
						roomNorth = False
					if i != 3:
						roomSouth = self.map[i + 1][j].containsPit
					else:
						roomSouth = False
					if j != 0:
						roomWest = self.map[i][j - 1].containsPit
					else:
						roomWest = False
					if j != 3:
						roomEast = self.map[i][j + 1].containsPit
					else:
						roomEast = False
					# If all four are not known to be wumpus-free, check to see if you can deduce one room to house the wumpus
					if (roomNorth == False and roomSouth == False and roomEast == False and roomWest == False) == False:
						if roomNorth == False and roomSouth == False and roomEast == False:
							self.map[i][j - 1].containsPit = True
						elif roomNorth == False and roomSouth == False and roomWest == False:
							self.map[i][j + 1].containsPit = True
						elif roomNorth == False and roomEast == False and roomWest == False:
							self.map[i + 1][j].containsPit = True
						elif roomSouth == False and roomEast == False and roomWest == False:
							self.map[i - 1][j].containsPit = True

		# Check for any newly found safe rooms
		for i in range(4):
			for j in range(4):
				self.map[i][j].checkIsSafe()

	def ask(self):
		# If the have located the gold but not picked it up, grab it and initiate departure sequence
		if self.pickUpGold == True:
			self.pickUpGold = False
			self.leaving = True
			return 'Grab'

		# If you do not have a current destination, find one
		if self.agentLocation == self.destination or self.destination is None:
			# Find a square that is known to be safe, but has not been visited
			foundNewRoom = False
			for i in range(4):
				for j in range(4):
					if self.map[i][j].isSafe == True and self.map[i][j].visited == False:
						self.destination = self.mapLocations[str(i) + str(j)]
						self.pathToDestination = self.find_best_path(self.agentLocation, self.destination)
						self.nextLocation = self.pathToDestination.popleft()
						foundNewRoom = True
						break;
			# If you did not find a new safe room, leave the cave
			if foundNewRoom == False:
				self.leaving = True

		# If you have the gold, set the exit as your destination and calculate the best path there
		if self.leaving == True and self.destination != 12:
			self.destination = 12
			self.pathToDestination = self.find_best_path(self.agentLocation, 12)
			self.nextLocation = self.pathToDestination.popleft()

		# If you are leaving the cave and are at the exit room, climb
		if self.leaving == True and self.agentLocation == 12:
			return 'Climb'

		# If you have a destination, but are not yet there
		if len(self.pathToDestination) != 0 or self.agentLocation != self.destination:
			# If you have reached square n in the path, start moving towards square n + 1
			if self.agentLocation == self.nextLocation:
				self.nextLocation = self.pathToDestination.popleft()

		

			# Find the direction of the next square
			agentLocationCoord = self.mapCoordinates[self.agentLocation]
			nextLocationCoord = self.mapCoordinates[self.nextLocation]
			if agentLocationCoord[1] == (nextLocationCoord[1] + 1):
				nextLocationDirection = 'L'
			elif agentLocationCoord[1] == (nextLocationCoord[1] - 1):
				nextLocationDirection = 'R'
			elif agentLocationCoord[0] == (nextLocationCoord[0] + 1):
				nextLocationDirection = 'U'
			elif agentLocationCoord[0] == (nextLocationCoord[0] - 1):
				nextLocationDirection = 'D'

			# If you are facing the direction of the next space, move forward
			if self.agentDirection == nextLocationDirection:
				self.prevLocation = self.agentLocation
				self.agentLocation = self.nextLocation
				return 'Forward'
			else:
				# Turn towards the direction of the next square
				if nextLocationDirection == 'L':
					if self.agentDirection == 'U':
						self.agentDirection = 'L'
						return 'TurnLeft'
					elif self.agentDirection == 'R':
						self.agentDirection = 'U'
						return 'TurnLeft'
					elif self.agentDirection == 'D':
						self.agentDirection = 'R'
						return 'TurnRight'
				elif nextLocationDirection == 'R':
					if self.agentDirection == 'U':
						self.agentDirection = 'R'
						return 'TurnRight'
					elif self.agentDirection == 'L':
						self.agentDirection = 'U'
						return 'TurnRight'
					elif self.agentDirection == 'D':
						self.agentDirection = 'R'
						return 'TurnLeft'
				elif nextLocationDirection == 'U':
					if self.agentDirection == 'L':
						self.agentDirection = 'U'
						return 'TurnRight'
					elif self.agentDirection == 'R':
						self.agentDirection = 'U'
						return 'TurnLeft'
					elif self.agentDirection == 'D':
						self.agentDirection = 'R'
						return 'TurnLeft'
				elif nextLocationDirection == 'D':
					if self.agentDirection == 'L':
						self.agentDirection = 'D'
						return 'TurnLeft'
					elif self.agentDirection == 'R':
						self.agentDirection = 'D'
						return 'TurnRight'
					elif self.agentDirection == 'U':
						self.agentDirection = 'R'
						return 'TurnRight'

	def find_best_path(self, startPoint, endPoint):
		# Build the adjacency matrix
		adjacMatrix = []
		for i in range(16):
			adjacMatrix.append( [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] )

		for i in range(4):
			for j in range(4):
				if self.map[i][j].isSafe != True:
					continue
				if i != 0:
					if(self.map[i - 1][j].isSafe == True):
						adjacMatrix[self.mapLocations[str(i) + str(j)]][self.mapLocations[str(i - 1) + str(j)]] = 1
				if i != 3:
					if(self.map[i + 1][j].isSafe == True):
						adjacMatrix[self.mapLocations[str(i) + str(j)]][self.mapLocations[str(i + 1) + str(j)]] = 1
				if j != 0:
					if(self.map[i][j - 1].isSafe == True):
						adjacMatrix[self.mapLocations[str(i) + str(j)]][self.mapLocations[str(i) + str(j - 1)]] = 1
				if j != 3:
					if(self.map[i][j + 1].isSafe == True):
						adjacMatrix[self.mapLocations[str(i) + str(j)]][self.mapLocations[str(i) + str(j + 1)]] = 1

		# Use dijkstra's algorithm to find the best path
		explored = set()
		frontier = ValueSortedDict()
		best_path = dict()
		prev_city = startPoint
		current_city = startPoint
		current_city_g = 0
		next_city_g = 0
		num_cities = 16
		target_city = endPoint

		frontier[startPoint] = 0
		best_path[startPoint] = [ [startPoint], startPoint ]

		while len(frontier) != 0:
			current_city, current_city_g = frontier.popitem(last = False)

			if current_city == target_city:
				return deque(best_path[current_city][0])

			explored.add(current_city)

			for next_city in range(num_cities):
				if next_city in explored:
					continue

				dist = adjacMatrix[current_city][next_city]

				if dist == 0:
					continue

				next_city_g = current_city_g + dist

				if next_city in best_path:
					if next_city_g < best_path[next_city][1]:
						current_path = copy.copy(best_path[current_city][0])
						current_path.append(next_city)
						best_path[next_city] = [ current_path, next_city_g ]
						frontier[next_city] = next_city_g
				else:
					current_path = copy.copy(best_path[current_city][0])
					current_path.append(next_city)
					best_path[next_city] = [ current_path, next_city_g ]
					frontier[next_city] = next_city_g

class jsamuel2_ExplorerAgent(ExplorerAgent):
	def __init__(self):
		super().__init__()
		self.kb = KB()

	def program(self, percept):
		self.kb.tell(percept)
		return self.kb.ask()