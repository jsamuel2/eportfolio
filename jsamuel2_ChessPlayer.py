from chess_player import ChessPlayer
import random
import copy

class Node():
	def __init__(self, board, color, move=None, isMax=False, depth=0):
			self.board = board						# The board object of the node
			self.color = color						# The color playing for this board
			self.move = move						# The move that resulted in this board
			self.isMax = max						# Max node if true, Min node is false, set to False initially so that the first tier will be a max node
			self.depth = depth						# The node's depth on the tree
			self.children = [ ]						# Contains all of the node's children

			# Give the node children, unless it is the third level on the tree (allows the tree to recursively fill itself)
			if depth < 2:
				self.populate_children()

	# Create a new node for each possible board resulting from the current state and add it to the current node's children
	def populate_children(self):
		legal_moves = self.board.get_all_available_legal_moves(self.color)
		for i in range(len(legal_moves)):
			child_board = copy.deepcopy(self.board)
			child_board.make_move(legal_moves[i][0], legal_moves[i][1])
			child_color = "white" if self.color == "black" else "black"
			child_move = legal_moves[i]
			child_isMax = True if self.isMax else False
			child_depth = self.depth + 1
			self.children.append(Node(child_board, child_color, child_move, child_isMax, child_depth))

	# Recursively returns the best possible move from the tree
	def solve(self):
		# If it is the root (max node), recursively solve each child node and return the move of the highest scored node
		if self.depth == 0:
			best_score = -2000000000
			best_moves = [ ]
			for i in range(len(self.children)):
				node_score = self.children[i].solve()
				if i == 0:
					best_score = node_score
					best_moves.append(self.children[i].move)
				else:
					if node_score > best_score:
						best_score = node_score
						best_moves = [ ]
						best_moves.append(self.children[i].move)
			return random.choice(best_moves)
		# If it is a direct child of the root (min node), recursively solve each child and return the lowest score
		if self.depth == 1:
			best_score = 2000000000
			for i in range(len(self.children)):
				node_score = self.children[i].solve()
				if i == 0:
					best_score = node_score
				else:
					if node_score < best_score:
						best_score = node_score
			return best_score
		# If it is in the frontier, simply return the score of the board
		else:
			return self.score_board()




	# Returns a score for each board based based on how many/which pieces you have on the board - your opponent's pieces
	def score_board(self):
		# Set the opponents color for easier future use
		if self.color == "black":
			opp_color = "white"
		else:
			opp_color = "black"

		# Starting score for the board
		score = 0

		# If you have the other player in checkmate, choose that move
		if self.board.is_king_in_checkmate(opp_color):
			return 1000000000
		# If the other player has you in checkmate, do not choose that move
		elif self.board.is_king_in_checkmate(self.color):
			return -1000000000
		# If the board is a stalemate, don't choose it unless you have to
		elif len(self.board.get_all_available_legal_moves(opp_color)) == 0:
			return -1000000
		# If you have the other player in check, it's most likely the move to make
		elif self.board.is_king_in_check(opp_color):
			score += 10000
		# If the other player has you in check, it's most likely not the move to make
		elif self.board.is_king_in_check(self.color):
			score -= 10000

		# Give/remove points for every piece on the board. +/- 1 point for pawns,
		# 3 for knights and bishops, 5 for rooks, 7 for crazy mode pieces and 9 for queens
		for piece in self.board.items():
			notation = piece[1].get_notation()
			if notation == 'p':
				if self.color == "black":
					score += 1
				else:
					score -= 1
			elif notation == 'n' or notation == 'b':
				if self.color == "black":
					score += 3
				else:
					score -= 3
			elif notation == 'r':
				if self.color == "black":
					score += 5
				else:
					score -= 5
			elif notation == 'f' or notation == 'p':
				if self.color == "black":
					score += 7
				else:
					score -= 7
			elif notation == 'q':
				if self.color == "black":
					score += 9
				else:
					score -= 9
			elif notation == 'P':
				if self.color == "black":
					score -= 1
				else:
					score += 1
			elif notation == 'N' or notation == 'B':
				if self.color == "black":
					score -= 3
				else:
					score += 3
			elif notation == 'R':
				if self.color == "black":
					score -= 5
				else:
					score += 5
			elif notation == 'F' or notation == 'P':
				if self.color == "black":
					score -= 7
				else:
					score += 7
			elif notation == 'Q':
				if self.color == "black":
					score -= 9
				else:
					score += 9
					
		return score

class jsamuel2_ChessPlayer(ChessPlayer):
	def __init__(self, board, color):
		super().__init__(board, color)

	def get_move(self, your_remaining_time, opp_remaining_time, prog_stuff):
		# Create a tree from the current gamestate
		tree = Node(self.board, self.color)

		# Return the best possible move
		return tree.solve()