Required Links:
https://github.com/tlaskiew/choreTracker

Artifacts:

1. Test Plan Document
This assignment was to create a document that laid out a detailed plan for
testing our Chore Tracking Application. We had to describe the basic description
of the project, how we would test each part of the project, how we would record
our testing data, and how to set up the testing environment. The big thing that
I took away from writing this document is how many little things need to be
tested before you can safely say a project is fully functional. You need to not
only make sure that something works, but also account for if it handles any
little thing that could potentially break it. There is not much that I wish we
would have done differently, but I do wish that we would have been further along
in our implementation before it was time to test the software.

2. Project Plan Document
The purpose of this assignment was to lay out a design plan for our system,
describe any tools we may need to implement it, and also set up a schedule to
complete the implementation. The big thing I took away from this document was
how difficult it is to set up a schedule. You have to not only account for how
long working on the actual project might take, but also for any external
distractions that may affect how quickly we can accomplish things. If we were
to redo this document today, I would spend a little more time getting familiar
with Android application development before designing the system. I had a good
idea of how I wanted the big picture pieces of the project to work, but I was
not familiar with how to plan out the smaller details until I learned a little
bit more about how application development really works.

3. Chore Tracking Application
The Chore Tracking Application is a system that allows parent and child accounts
to interact in a way that parents can post incentivized chores and prizes for
their children. The children then complete those chores for points, and use the
points to redeem rewards. I learned a lot of things while working on this
project. I learned how to develop Android applications, how to work on larger
projects in a group environment, and how to really go through the phases of the
design process instead of just winging it. There were also a couple of things
that I wish we would have done differently. I wish we would have met as a group
to go over more of the small details such as UI design and I wish that we would
have done something to try and get the less-involved members of the group
contributing more to the implementation and design of the system.

4. Chess Player Algorithm
This was one of the most fun pieces of code that I got to write throughout my
time at UMW. We had to implement a class that returned a move for a chess game.
We received methods to get all of our available moves and some other basic
information from the board, and we had to design an algorithm that decided the
best one. I developed a mini-max algorithm that builds a tree-like structure
with arrays and scores each potential board based off of how many pieces
remained on the board after the move, and if the move put me in any potential
danger from the opposing player's next move. The big thing that I would have
done differently is implemented alpha-beta pruning to allow the code to look
through the potential moves faster, since my algorithm was a little bit slow.
From this project I learned a lot about the trade-off between optimization and
efficiency.

5. Wumpus World Algorithm
This was another really fun algorithm to write. Wumpus world is a game where you
get put into the bottom-left corner of a 4 x 4 grid-style room that contains
randomly placed pits, walls, one treasure chest and one wumpus monster. You are
completely blind to your surroundings and you only know that pits create a
breeze in all adjacent squares, you feel a bump if you try to move into a wall,
the treasure chest creates glitter in all adjacent squares and the wumpus create
a stench in all adjacent squares. The objective of the game if to find the
treasure and then leave through the space you entered without dying. I learned
a lot from this project about keeping track of multiple things in a knowledge
base and how to really spend a lot of time creating a clever program that
accounts for a lot of little things and makes many different calculations to
influence a decision. There was not much a wish a did differently about my
alogorithm design, but time constraints did keep me from being able to fix a
couple of bugs that crippled the efficiency of the program. I think that if I
was able to fix those, I would have been able to create a really effective
algorithm.

Reflective Essay:
1. I think that the single most useful lesson that I learned throughout this project
is one of the biggest lessons that needs to be learned in college: make sure that
you have a really good, thorough plan and don't procrastinate it. It's a lesson
that I have learned in previous semesters, so we did not have too many issues
with procrastinating this project. However, it was a project that I was a little
worried about going into, so we started early and thought everything through and
our experience was not too bad, which helped to prove the lessons I have already
learned to be correct.

2. I think that almost every skill I've learned throughout college came in handy
for this project. Whether it was actually knowing how to write code, how to design
a system, or how to google around and figure out an issue, I was able to apply every
lesson and skill that I have learned in the past 3 years at UMW and apply it to
successfully pull off this project.

3. I think that the only real problem we experienced in this project was that half
of our group did not come to class or come through on anything that they said they
would do. I was able to personally overcome by indentifying the strengths and
weaknesses of the group (which was pretty much that Troy worked very hard and the
other's did not) and try to split up the work in a way that would not cripple the
entire project when the others in the group did not come through for us.

4. I think that there are two areas I would like to continue to strengthen my
skills after this project. I thought developing the App itself was actually
kind of fun and not as hard as I thought it would be, so I would like to
learn more about application development. I could also probably work on
my leadership skills a little more and find ways to involve the other members
of the group a little bit more.

5. I did not really learn anything significant about myself through this project.
It was surprisingly simple and easy to implement. Before it all started, I thought
that it would be a big challenge to do, but after we got started it was smooth
sailing all the way through. Therefore, I would not say that I had any big self-
revelations, but I am proud of the work that some of our group put in to make
it happen.

6. This experience has had no impact on my post-graduation plans. In the near future
if I find myself needing something to do with my spare time I may go buy an
application development book online and learn a little more about the subject, but
I don't plan on starting up or joining any big application development teams or
anything like that.
